package com.casallas.futbol;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView equipos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        equipos=findViewById(R.id.equipos);

        String[] misEquipos;
        misEquipos=getResources().getStringArray(R.array.equipos);//se toma de values

        ArrayAdapter cargadorEquipos;

        cargadorEquipos=new ArrayAdapter(this, R.layout.equipo, misEquipos);


        //Asociar el ListView al adaptor
        equipos.setAdapter(cargadorEquipos);

        //Definir nuestro adaptador como un listener
        equipos.setOnItemClickListener(this);


    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String nombreEquipo;

        nombreEquipo=  ((TextView) view).getText().toString();

        Toast.makeText(getBaseContext(),nombreEquipo, Toast.LENGTH_LONG).show();
        Snackbar.make(findViewById(R.id.equipos), nombreEquipo, Snackbar.LENGTH_LONG).show();

        //Crear un Intent (Un objeto que maneja la intención de hacer peticiones a otras actividades)
        Intent objetivo;
        objetivo=new Intent(getBaseContext(), InfoEquipo.class);

        //Pasarle un dato a la otra actividad

        objetivo.putExtra("nombre",nombreEquipo);
        startActivity(objetivo);
    }
}